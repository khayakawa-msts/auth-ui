import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustController } from './cust/cust/cust.controller';
import { CustModule } from './cust/cust.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MercController } from './merc/merc/merc.controller';
import { MercModule } from './merc/merc.module';
import { CardController } from './card/card/card.controller';
import { CardModule } from './card/card.module';
import { PromptService } from './prompt/prompt.service';
import { PromptController } from './prompt/prompt.controller';
import { PromptModule } from './prompt/prompt.module';
import { ProductModule } from './product/product.module';
import { FeeController } from './fee/fee.controller';
import { FeeService } from './fee/fee.service';
import { FeeModule } from './fee/fee.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [CustModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'oracle',
      connectString: process.env.DB_HOST,
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      logging: true,
    }),
    MercModule,
    CardModule,
    PromptModule,
    ProductModule,
    FeeModule,
  ],
  controllers: [AppController, CustController, MercController, CardController, PromptController, FeeController],
  providers: [AppService, PromptService, FeeService],
})
export class AppModule {}
