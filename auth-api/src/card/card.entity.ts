import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({name: 'CARD'})
export class Card {
  @PrimaryColumn({name:'CUST_NO'})
  cust_no: number

  @PrimaryColumn({name:'CARD_NO'})
  card_no: number

  @Column({name:'TRACK_2'})
  track_2: string

  @Column({name:'PIN_NO'})
  pin_no: string

  @Column({name:'UNIT_NO'})
  unit_no: string
}