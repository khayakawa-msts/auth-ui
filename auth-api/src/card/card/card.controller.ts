import { Controller, Get, Param } from '@nestjs/common';
import { Card } from '../card.entity';
import { CardService } from '../card.service';

@Controller('card')
export class CardController {
    constructor(private cardService: CardService){}

    @Get('cust_no/:cust_no')
    getCust(@Param('cust_no') cust_no): Promise<Card[]> {
        return this.cardService.findByCustNo(cust_no);
    }
}
