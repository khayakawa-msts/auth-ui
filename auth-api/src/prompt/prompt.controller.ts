import { Controller, Get, Param } from '@nestjs/common';
import { Prompt } from './prompt.entity';
import { PromptService } from './prompt.service'

@Controller('prompt')
export class PromptController {
    constructor(private promptService: PromptService){}

    @Get('cust_no/:cust_no')
    getCust(@Param('cust_no') cust_no): Promise<Prompt[]> {
        return this.promptService.getCust(cust_no);
    }

    @Get('cust_no/:cust_no/card_no/:card_no')
    getCard(@Param('cust_no') cust_no, @Param('card_no') card_no): Promise<Prompt[]> {
        return this.promptService.getCard(cust_no, card_no);
    }
}
