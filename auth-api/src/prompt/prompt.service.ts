import { Injectable } from '@nestjs/common';
import { Prompt } from './prompt.entity'
import { getManager } from 'typeorm'

@Injectable()
export class PromptService {
    async getCust(cust_no): Promise<Prompt[]> {
        const custNo = Number(cust_no);
        const entityManager = getManager()
        return await entityManager.query(
            "select cu.card_opt, " +
            "decode(cu.card_opt, 0, 'HUB', 1, 'PIN', 2, 'PO', 3, 'Unit', 4, 'Trip') opt, " +
            "decode(cu.card_opt_setting, 0, 'Yes', 1, 'No', 2, 'Optional') setting " +
            "from cust_level_card_opt cu " +
            "where cu.cust_no=:cust_no " +
            "order by cu.card_opt", [cust_no])
    }

    async getCard(cust_no, card_no): Promise<Prompt[]> {
        const custNo = Number(cust_no);
        const cardNo = Number(card_no);
        const entityManager = getManager()
        return await entityManager.query(
            "select " +
            "    card_opt, " +
            "    decode(card_opt, 0, 'HUB', 1, 'PIN', 2, 'PO', 3, 'Unit', 4, 'Trip') opt, " +
            "    decode(card_opt_setting, 0, 'Yes', 1, 'No', 2, 'Optional') setting " +
            "from ( " +
            "    select cu.card_opt, cu.card_override_flag, " +
            "    case cu.card_override_flag " +
            "        when 'Y' then " +
            "            coalesce(ca.card_opt_setting, cu.card_opt_setting) " +
            "        ELSE " +
            "            cu.card_opt_setting " +
            "    END card_opt_setting " +
            "    from cust_level_card_opt cu " +
            "    left join card_level_card_opt ca on cu.cust_no=ca.cust_no and cu.card_opt=ca.card_opt " +
            "    where cu.cust_no=:cust_no and (ca.card_no is null or ca.card_no=:card_no) " +
            ") " +
            "order by card_opt", [cust_no, card_no])
    }

}
