import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({name: 'card_level_card_opt'})
export class Prompt {
  @PrimaryColumn({name:'CARD_OPT'})
  card_opt: number

  @Column({name:'OPT'})
  opt: string

  @Column({name: 'SETTING'})
  setting: string
}