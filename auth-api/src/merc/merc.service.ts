import { Injectable } from '@nestjs/common';
import { Repository, getRepository, getManager } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Merc } from './merc.entity';
import { resolve } from 'dns';

@Injectable()
export class MercService {
    constructor(
        @InjectRepository(Merc)
        private mercRepository: Repository<Merc>,
    ) { }

    async findAll(): Promise<Merc[]> {
        return await this.mercRepository.find();
    }

    async find(merc_no): Promise<Merc> {
        let mercNo = Number(merc_no);
        const entityManager = getManager()
        return await entityManager.query(
            "select m.merc_no, m.name, ml.trans_currency, " +
            "decode(ml.quantity_type, 0, 'GAL', 'LTR') quantity_type " +
            "from merc m " +
            "join merc_location ml on m.merc_no=ml.merc_no " +
            "where m.merc_no=:merc_no", [merc_no])
    }

    async findByType(merc_type): Promise<Merc[]> {
        let mercType = Number(merc_type);
        const entityManager = getManager()
        return await entityManager.query(
            `select distinct decode(:merc_type, 2, mca.external_merc_id, m.merc_no) merc_no, m.name, ml.trans_currency,
            decode(ml.quantity_type, 0, 'GAL', 'LTR') quantity_type
            from merc m
            join merc_location ml on m.merc_no=ml.merc_no
            join merc_card_acceptance mca on m.merc_no=mca.merc_no
            join r_merc_type_card_type_map map on mca.card_type=map.card_type and map.merc_type=:merc_type
            where (m.merc_type=:merc_type and m.status=0) or m.merc_no=1111
            order by merc_no`, [merc_type])
    }
}
