import { Test, TestingModule } from '@nestjs/testing';
import { MercController } from './merc.controller';

describe('Merc Controller', () => {
  let controller: MercController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MercController],
    }).compile();

    controller = module.get<MercController>(MercController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
