import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({name: 'MERC'})
export class Merc {
  @PrimaryColumn({name:'MERC_NO'})
  merc_no: number

  @Column({name:'NAME'})
  name: string

  @Column({name: 'TRANS_CURRENCY'})
  trans_currency: string

  @Column({name: 'QUANTITY_TYPE'})
  quantity_type: string
}