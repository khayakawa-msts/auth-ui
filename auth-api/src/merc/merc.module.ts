import { Module } from '@nestjs/common';
import { MercService } from './merc.service';
import { MercController } from './merc/merc.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Merc } from './merc.entity';

@Module({})
@Module({
  imports: [
    TypeOrmModule.forFeature([Merc]),
  ],
  providers: [MercService],
  controllers: [MercController],
  exports: [MercService],
})
export class MercModule {}
