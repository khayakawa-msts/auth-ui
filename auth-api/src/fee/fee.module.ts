import { Module } from '@nestjs/common';
import { FeeController } from './fee.controller'
import { FeeService } from './fee.service'

@Module({
    imports: [],
    providers: [FeeService],
    controllers: [FeeController],
    exports: [FeeService],
})
export class FeeModule {}
