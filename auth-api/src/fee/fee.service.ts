import { Injectable } from '@nestjs/common';
import { getManager } from 'typeorm';

@Injectable()
export class FeeService {
  async getDescriptions(): Promise<string[]> {
    const entityManager = getManager()
    const ret = await entityManager.query('select fee_description from r_quikq_fee_description order by fee_description_type')
    const reducer = (a, b) => {
      a.push(b.FEE_DESCRIPTION)
      return a
    }
    return ret.reduce(reducer, [])
  }

  async getCategories(): Promise<string[]> {
    const entityManager = getManager()
    const ret = await entityManager.query('select fee_category from r_quikq_fee_category where category_type!=4 order by category_type')
    const reducer = (a, b) => {
      a.push(b.FEE_CATEGORY)
      return a
    }
    return ret.reduce(reducer, [])
  }
}
