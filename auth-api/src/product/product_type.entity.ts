import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({name: 'product_type'})
export class ProductType {
  @PrimaryColumn({name:'PRODUCT_TYPE'})
  product_code: number

  @Column({name:'DESCRIPTION'})
  description: string
}