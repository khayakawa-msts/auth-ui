import { Controller, Get, Param } from '@nestjs/common'
import { ProductService } from './product.service'
import { Product } from './product.entity'
import { ProductUom } from './product_uom.entity'
import { ProductType } from './product_type.entity'

@Controller('product')
export class ProductController {
  constructor(private productService: ProductService){}

  @Get('type/:type')
  getProducts(@Param('type') type): Promise<Product[]> {
    return this.productService.getProducts(type)
  }

  @Get(':prod_code/uom')
  getUoms(@Param('prod_code') prod_code): Promise<ProductUom[]> {
    return this.productService.getUom(prod_code)
  }

  @Get(':prod_code/type')
  getProductTypes(@Param('prod_code') prod_code): Promise<ProductType[]> {
    return this.productService.getProductTypes(prod_code)
  }
}
