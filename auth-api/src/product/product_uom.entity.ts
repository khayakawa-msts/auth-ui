import { Entity, PrimaryColumn } from 'typeorm';

@Entity({name: 'r_quantity_type_ri'})
export class ProductUom {
  @PrimaryColumn({name:'UOM'})
  uom: string
}