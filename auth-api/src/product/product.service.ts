import { Injectable } from '@nestjs/common'
import { Product } from './product.entity'
import { ProductType } from './product_type.entity'
import { ProductUom } from './product_uom.entity'
import { getManager } from 'typeorm'

@Injectable()
export class ProductService {
  async getProducts(type): Promise<Product[]> {
    const entityManager = getManager()
    if (type == 0) {
      return await entityManager.query('select product_code, description from r_auth_product_code order by product_code')
    }
    if (type == 2) {
      return await entityManager.query('select external_code product_code, external_desc description from r_auth_external_product_map where external_id=1 order by external_code')
    }
  }

  async getUom(product_code): Promise<ProductUom[]> {
    const entityManager = getManager()
    const qry='select ' +
      '  case quantity_type ' +
      "    when 0 then 'GAL' " +
      "    when 1 then 'LTR' " +
      "    when 2 then 'QRT' " +
      "    when 3 then 'ECH' " +
      "    when 6 then 'DGE' " +
      "    when 7 then 'GGE' " +
      "    when 8 then 'DLE' " +
      "    when 9 then 'GLE' " +
      "    when 10 then 'KGS' " +
      "    when 11 then 'LBS' " +
      '  end uom ' +
      'from r_auth_product_code ap ' +
      'join r_auth_product_item_code_map map1 on ap.product_code=map1.product_code ' +
      'join r_item i on map1.item_code=i.item_code ' +
      'join r_item_code_quantity_type_map map2 on i.item_code=map2.item_code ' +
      'where ap.product_code=:prod_code ' +
      'order by map2.quantity_type'
    return await entityManager.query(qry, [product_code])
  }

  async getProductTypes(product_code): Promise<ProductType[]> {
    const entityManager = getManager()
    const qry='select t.product_type, t.description ' +
      'from r_auth_product_code_type_map m ' +
      'join r_auth_product_type t on m.product_type = t.product_type ' +
      'where m.product_code = :prod_code ' +
      'order by t.product_type'
    return await entityManager.query(qry, [product_code])
  }
}
