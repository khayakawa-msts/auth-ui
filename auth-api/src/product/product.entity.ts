import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({name: 'r_auth_product_code'})
export class Product {
  @PrimaryColumn({name:'PRODUCT_CODE'})
  product_code: number

  @Column({name:'DESCRIPTION'})
  description: string
}