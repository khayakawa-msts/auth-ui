import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({name: 'CUST'})
export class Cust {
  @PrimaryColumn({name:'CUST_NO'})
  cust_no: number;

  @Column({name:'NAME'})
  name: string;
}