import { Injectable } from '@nestjs/common';
import { Repository, getRepository, getManager } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Cust } from './cust.entity';

@Injectable()
export class CustService {
    constructor(
        @InjectRepository(Cust)
        private custRepository: Repository<Cust>,
    ) { }

    async findAll(): Promise<Cust[]> {
        return await this.custRepository.find();
    }

    async find(cust_no): Promise<Cust> {
        let custNo = Number(cust_no);
        const cust = await getRepository(Cust)
            .createQueryBuilder("cust")
            .where("cust_no=:cust_no", {cust_no: custNo})
            .getOne();
        return cust;
    }

    async findByType(cust_type): Promise<Cust[]> {
        let entityManager = getManager()
        const qry = `select * from (
                select decode(:cust_type, 2, ca.ext_card_id, c.cust_no) cust_no, c.name from cust c
                join (select distinct cust_no, ext_card_id from card where track_2 is not null and status=0 and length(track_2) > 1) ca on c.cust_no=ca.cust_no
                join cust_credit cc on c.cust_no=cc.cust_no
                where c.status=0 and cc.credit_limit > 1000 and c.customer_type=:cust_type
            ) where cust_no is not null
            order by cust_no`
        return await entityManager.query(qry, [cust_type])
    }
}
