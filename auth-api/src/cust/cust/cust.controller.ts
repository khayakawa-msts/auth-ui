import { Controller, Get, Param } from '@nestjs/common';
import { Cust } from '../cust.entity';
import { CustService } from '../cust.service';

@Controller('cust')
export class CustController {
    constructor(private custService: CustService){}

    @Get()
    index(): Promise<Cust[]> {
        return this.custService.findAll();
    }

    @Get('no/:cust_no')
    getCust(@Param('cust_no') cust_no): Promise<Cust> {
        return this.custService.find(cust_no);
    }

    @Get('type/:type')
    getCustByType(@Param('type') cust_type) {
        return this.custService.findByType(cust_type);
    }
}
