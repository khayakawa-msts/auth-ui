import { Module } from '@nestjs/common';
import { CustService } from './cust.service';
import { CustController } from './cust/cust.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cust } from './cust.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Cust]),
  ],
  providers: [CustService],
  controllers: [CustController],
  exports: [CustService],
})
export class CustModule {}
